package com.androidjow.amine.sudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class JeuActivity extends AppCompatActivity {

    Grille grille;
    Integer x,y,recVal,recX,recY;
    Intent goChoix;
    String oldGrille = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_jeu);

        //la grille
        grille = (Grille) findViewById(R.id.gridView);

        //les intents
        goChoix = new Intent(this,ChoixActivity.class);
        recVal  = getIntent().getIntExtra("CHOIX",-1);
        recX    = getIntent().getIntExtra("X",-1);
        recY    = getIntent().getIntExtra("Y",-1);
        oldGrille = getIntent().getStringExtra("GRILLE");

        //mettre a jour la grille
        if(oldGrille != null){
            grille.maj(oldGrille);
        }

        //mettre a jour la case modifier
        if(recVal != -1){
            grille.set(recY,recX,recVal);
        }



        grille.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //la ligne
                x = grille.getXFromMatrix((int) event.getY());
                //la colonne
                y = grille.getYFromMatrix((int) event.getX());
                //si c'est une case valide
                if( grille.valideCase( grille.getXFromMatrix(x),grille.getYFromMatrix(y) ) ){
                    //si ce n'est pas une case fixe
                    if(grille.isNotFix(y,x)){
                        //Log.i("NFIX","NOT FIXE");
                        goChoix.putExtra("X", x);
                        goChoix.putExtra("Y", y);
                        goChoix.putExtra("GRILLE",grille.get());
                        startActivity(goChoix);
                    }
                    //Log.i("OK","VALIDE");
                }
                return true;
            }
        });
    }

    public void verifyValidity(View view){
        Log.i("GSTR2","clicked");
        if(grille.gagne()){
            Log.i("GSTR2","gagne");
            grille.ok();
        }else{
            Log.i("GSTR2","not gagne");
            grille.ko();
        }
    }

    public void retourMain(View view){
        Intent ret = new Intent(this,MainActivity.class);
        startActivity(ret);
    }

}
