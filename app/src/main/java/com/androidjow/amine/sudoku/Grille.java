package com.androidjow.amine.sudoku;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.io.Serializable;

public class Grille extends View implements Serializable {

    private int screenWidth;
    private int screenHeight;
    private int n;
    private boolean ok = false,ko = false;

    private Paint paint1;   // Pour dessiner la grille (lignes noires)
    private Paint paint2;   // Pour le texte des cases fixes
    private Paint paint3;   // Pour dessiner les lignes rouges (grosse)
    private Paint paint4;   // Pour le texte noir des cases a modifier
    private Paint paint5;   // OK
    private Paint paint6;   // KO

    private int[][] matrix = new int[9][9];
    private boolean[][] fixIdx = new boolean[9][9];

    public Grille(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Grille(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Grille(Context context) {
        super(context);
        init();
    }

    private void init() {
        //Grille de depart
        set("000105000140000670080002400063070010900000003010090520007200080026000035000409000");

        // Grille Gagnante
        //set("672145398145983672389762451263574819958621743714398526597236184426817935831459267");

        // Grille Perdante
        //set("672145198145983672389762451263574819958621743714398526597236184426817935831459267");

        //les lignes horizontales et verticales
        paint1 = new Paint();
        paint1.setAntiAlias(true);
        paint1.setColor(Color.BLACK);// Couleur noire
        paint1.setTextSize(60);
        paint1.setStrokeWidth(10);

        //le remplissage automatique de la grille
        paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(Color.RED);// Couleur rouge
        paint2.setTextSize(60);// Taille du texte
        paint2.setTextAlign(Paint.Align.CENTER);// Centre le texte

        //les traits de separation rouge et gras
        paint3 = new Paint();
        paint3.setAntiAlias(true);
        paint3.setColor(Color.BLUE);// Couleur rouge
        paint3.setStrokeWidth(20);//Grosses lignes

        //le remplissage de l'utilisateur
        paint4 = new Paint();
        paint4.setAntiAlias(true);
        paint4.setColor(Color.BLACK);// Couleur noir
        paint4.setTextSize(60);// Taille du texte
        paint4.setTextAlign(Paint.Align.CENTER);// Centre le texte

        //KO
        paint5 = new Paint();
        paint5.setAntiAlias(true);
        paint5.setColor(Color.GREEN);// Couleur rouge
        paint5.setStrokeWidth(20);//Grosses lignes

        //OK
        paint6 = new Paint();
        paint6.setAntiAlias(true);
        paint6.setColor(Color.RED);// Couleur rouge
        paint6.setStrokeWidth(20);//Grosses lignes

    }

    @Override
    protected void onDraw(Canvas canvas) {
        screenWidth = getWidth();
        screenHeight = getHeight();
        int w = Math.min(screenWidth, screenHeight);
        w = w - (w%9);
        n = w / 9 ;

        // Dessiner les lignes verticales et les lignes horizontales
        for(int i=0;i<=w;i+=n){
                canvas.drawLine(i, 0, i, w, paint1);
                canvas.drawLine(0, i, w, i, paint1);
        }

        // Dessiner 2 lignes rouges verticales et 2 lignes rouges horizontales
        for(int i=3*n;i<=6*n;i+=3*n){
            canvas.drawLine(i, 0, i, w, paint3);
            canvas.drawLine(0, i, w, i, paint3);
        }

        if(ok){
            // OK GREEN
            for(int i=0;i<=9*n;i+=9*n){
                canvas.drawLine(i, 0, i, w, paint5);
                canvas.drawLine(0, i, w, i, paint5);
            }
        }

        if(ko){
            // KO RED
            for(int i=0;i<=9*n;i+=9*n){
                canvas.drawLine(i, 0, i, w, paint6);
                canvas.drawLine(0, i, w, i, paint6);
            }
        }

        // Les contenus des cases
        String s;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                s = "" + (matrix[j][i] == 0 ? "" : matrix[j][i]);
                if (fixIdx[j][i])
                    canvas.drawText(s, i * n + (n / 2) - (n / 10), j * n
                            + (n / 2) + (n / 10), paint2);
                else
                    canvas.drawText(s, i * n + (n / 2) - (n / 10), j * n
                            + (n / 2) + (n / 10), paint1);
            }
        }
    }

    public int getXFromMatrix(int x) {
        // Renvoie l'indice d'une case a partir du pixel x de sa position h
        return (x / n);
    }

    public int getYFromMatrix(int y) {
        // Renvoie l'indice d'une case a partir du pixel y de sa position v
        return (y / n);
    }

    public void set(String s, int i) {
        // Remplir la ieme ligne de la matrice matrix avec un vecteur String s
        int v;
        for (int j = 0; j < 9; j++) {
            v = s.charAt(j) - '0';
            matrix[i][j] = v;
            if (v == 0)
                fixIdx[i][j] = false;
            else
                fixIdx[i][j] = true;
        }
    }

    public void set(String s) {
        // Remplir la matrice matrix a partir d'un vecteur String s
        for (int i = 0; i < 9; i++) {
            set(s.substring(i * 9, i * 9 + 9), i);
        }
    }

    public void set(int x, int y, int v) {
        // Affecter la valeur v a la case (y, x)
        // y : ligne
        // x : colonne
        matrix[y][x] = v;
    }

    public String get(){
        // Les contenus des cases
        String s = "";
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                s += Integer.valueOf(matrix[i][j]);
            }
        }
        return s;
    }

    public boolean isNotFix(int x, int y) {
        // Renvoie si la case (y, x) n'est pas fixe
        // A completer
        if(!fixIdx[y][x]) return true;
        return false;
    }

    public boolean gagne() {
        // Verifier si la case n'est pas vide ou bien s'il existe
        // un numero double dans chaque ligne ou chaque colonne de la grille
        for (int v = 1; v <= 9; v++) {
            for (int i = 0; i < 9; i++) {
                boolean bx = false;
                boolean by = false;
                for (int j = 0; j < 9; j++) {
                    if (matrix[i][j] == 0) return false;
                    if ((matrix[i][j] == v) && bx) return false;
                    if ((matrix[i][j] == v) && !bx) bx=true;
                    if ((matrix[j][i] == v) && by) return false;
                    if ((matrix[j][i] == v) && !by) by=true;
                }
            }
        }
        // ------
        // Gagne
        return true;
    }

    //Verifier si c'est une case valide
    public boolean valideCase(int x,int y){
        if((x >= 0 && x <= 8)&&(y >= 0 && y <= 8)) return true;
        return false;
    }

    //mise a jour de la grille sans affecter FixId
    public boolean maj(String s){
        int v;
        for (int i = 0,k=0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                v = s.charAt(k++) - '0';
                matrix[i][j] = v;
            }
        }
        return true;
    }

    public void ok(){
        ok = true;
        this.invalidate();
    }

    public void ko(){
        ko = true;
        this.invalidate();
    }


}