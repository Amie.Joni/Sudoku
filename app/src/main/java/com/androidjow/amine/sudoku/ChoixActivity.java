package com.androidjow.amine.sudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView;

public class ChoixActivity extends AppCompatActivity {

    ListView choixListView;
    int x,y;
    Intent choixIntent;
    String grille;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix);

        choixListView = findViewById(R.id.choixListView);
        Integer[] Nbr = {0,1,2,3,4,5,6,7,8,9};
        ArrayAdapter<Integer> myAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,Nbr);
        choixListView.setAdapter(myAdapter);
        choixListView.setOnItemClickListener(listClick);
    }

    private AdapterView.OnItemClickListener listClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            choixIntent = new Intent(getBaseContext(),JeuActivity.class);
            Integer value = (Integer) choixListView.getItemAtPosition(position);

            //Log.i("VAL",value.toString());
            x = getIntent().getIntExtra("X",-1);
            y = getIntent().getIntExtra("Y",-1);
            grille = getIntent().getStringExtra("GRILLE");
            choixIntent.putExtra("GRILLE", grille);
            choixIntent.putExtra("CHOIX",value);
            choixIntent.putExtra("X", x);
            choixIntent.putExtra("Y", y);

            startActivity(choixIntent);
        }
    };
}
