package com.androidjow.amine.sudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Intent goJeu,goAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button JouButton = findViewById(R.id.JouButton);
        Button retButton = findViewById(R.id.appButton);

        goJeu = new Intent(this,JeuActivity.class);
        goAbout = new Intent(this,AboutActivity.class);

        JouButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(goJeu);
            }
        });

        retButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(goAbout);
            }
        });
    }
}
